import { Controller,Get, Param, Post, Body, Delete, Query } from '@nestjs/common';
import { ContactsService } from './contacts.service';
//import { CreatContactsDto } from './create-contact.dto';

@Controller('contacts')
export class ContactsController {
    constructor(private contactsService: ContactsService) {}

    @Get()
    async getContacts() {
        const contacts = await this.contactsService.getContacts();
        console.log("Contacts-list-->",contacts);
    }

    @Get(':courseId')
    async getContact(@Param('courseId') courseId) {
        const contact = await this.contactsService.getContact(courseId);
        console.log("Contact-single-->",contact);
        return contact;
    }

    // @Post()
    // async addContact(@Body() createCourseDto: CreatContactsDto) {
    //     const contact = await this.contactsService.addContact(createCourseDto);
    //     return contact;
    // }

    @Delete()
    async deleteContact(@Query() query) {
        const contacts = await this.contactsService.deleteContact(query.courseId);
        console.log("Contact-Delete-->",contacts);
        return contacts;
    }
}
