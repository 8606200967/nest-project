import { Injectable } from '@nestjs/common';
import { CONTACTS } from './contacts.mock';

@Injectable()
export class ContactsService {
    contacts = CONTACTS;
    getContacts(): Promise<any> {
        return new Promise(resolve => {
            resolve(this.contacts);
        });
    }

    getContact(courseId): Promise<any> {
        let id = Number(courseId);
        return new Promise(resolve => {
            const contact = this.contacts.find(contact => contact.id === id);
            if (!contact) {
                //throw new HttpException('Course does not exist', 404)
                alert("Contacts does not exist")
            }
            resolve(contact);
        });
    }
    addContact(contact): Promise<any> {
        return new Promise(resolve => {
            this.contacts.push(contact);
            resolve(this.contacts);
        });
    }
    deleteContact(contactId): Promise<any> {
        let id = Number(contactId);
        return new Promise(resolve => {
            let index = this.contacts.findIndex(contact => contact.id === id);
            if (index === -1) {
                //throw new HttpException('Contact does not exist', 404);
                alert("Contacts does not exist")
            }
            this.contacts.splice(index, 1);
            resolve(this.contacts);
        });
    }
}
